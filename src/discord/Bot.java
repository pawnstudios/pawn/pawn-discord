/*
 *  Copyright (c) Alex 2022
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 */

package discord;

import it.gotoandplay.smartfoxserver.data.User;
import it.gotoandplay.smartfoxserver.events.InternalEventObject;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import it.gotoandplay.smartfoxserver.extensions.AbstractExtension;

import javax.security.auth.login.LoginException;

public class Bot extends AbstractExtension {
    private JDABuilder builder;
    private JDA bot;
    private int userCount = -1;

    public static void main(String[] args) {
        new Bot();
    }

    public Bot() {
        try {
            builder = new JDABuilder(System.getProperty("DISCORD_BOT_TOKEN"));
            builder.setActivity(Activity.playing("Pawn"));
            bot = builder.build();
        } catch (LoginException e) {
            e.printStackTrace();
        }

    }

    public synchronized void updateUserOnlineCount(int count) {
        if (count != userCount) {
            bot.getPresence().setActivity(Activity.playing("Pawn - " + count + " Online."));
            userCount = count;
        }
    }

    @Override
    public void handleInternalEvent(InternalEventObject internalEventObject) {

    }

    @Override
    public void handleRequest(String s, ActionscriptObject actionscriptObject, User user, int i) {

    }

    @Override
    public void handleRequest(String s, String[] strings, User user, int i) {

    }
}
